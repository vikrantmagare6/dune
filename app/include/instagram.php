<div id="instagram" class="instagram wow fadeInUp">
    <div class="instagram-title">
        <div class="inst-title">
            instagram
        </div>
    </div>
    
    <div class="insta-feed">

        <?php
        $access_token="2141705468.1677ed0.44f312c3c40a4bf082b04d69091ea7f2";
        $photo_count=5;

        $json_link="https://api.instagram.com/v1/users/self/media/recent/?";
        $json_link.="access_token={$access_token}&count={$photo_count}";

        $json = file_get_contents($json_link);


        $obj = json_decode($json);
        $data = $obj->data;
        


        ?>
        <div class="insta-feed-wrapp clearfix">
            <?php foreach ($data as $key => $value): ?>
                <?php 
                $rest = substr($value->caption->text, 0, 21);
                //var_dump($rest); ?>
                <div class="insta-content">
                    <a target="_blank" href="<?php echo $value->link; ?>" style="background-image: url(<?php echo $value->images->standard_resolution->url; ?>);" >
                        <img src="<?php echo $value->images->standard_resolution->url; ?>"  />
                        <div class="overlay">
                            <div class="inner">
                                <div class="inner-n">

                                    <?php if ($value->likes->count): ?>
                                        <span class="like">
                                            <i class="fa fa-heart-o"></i>
                                            <?php echo $value->likes->count; ?>    
                                        </span>
                                    <?php endif ?>
                                    <span class="comments">
                                        <i class="fa fa-comment-o"></i>

                                        <?php echo $value->comments->count; ?>
                                    </span>
                                    <span class="fullname">
                                        <?php echo $rest; ?>
                                        <?php echo $value->user_has_liked.'...'; ?>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

            <?php endforeach ?>


        </div>

        <?php



        ?>                          
    </div>
</div>