
<div id="locate-us" class="locate-us calcscrolldiv ">
    <div class="inner-div">
        <span class="processdiv_i inview locate-us-title">
            <h3>locate us</h3>
            <div class="l-1"><i></i></div>

            <div class="l-3"><i></i></div>    
            <div class="l-4"><i></i></div>
        </span>
        <div class="locate-us-list">
            <div class="top">

                <?php 

                function get_client_ip() {
                    $ipaddress = '';
                    if (getenv('HTTP_CLIENT_IP'))
                        $ipaddress = getenv('HTTP_CLIENT_IP');
                    else if(getenv('HTTP_X_FORWARDED_FOR'))
                        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
                    else if(getenv('HTTP_X_FORWARDED'))
                        $ipaddress = getenv('HTTP_X_FORWARDED');
                    else if(getenv('HTTP_FORWARDED_FOR'))
                        $ipaddress = getenv('HTTP_FORWARDED_FOR');
                    else if(getenv('HTTP_FORWARDED'))
                        $ipaddress = getenv('HTTP_FORWARDED');
                    else if(getenv('REMOTE_ADDR'))
                        $ipaddress = getenv('REMOTE_ADDR');
                    else
                        $ipaddress = 'UNKNOWN';
                    return $ipaddress;
                }

                $ip =  get_client_ip();






                $url = "http://api.ipinfodb.com/v3/ip-city/?key=253421e7423da701512b73944e4415bda561b0d38545074cadca221a843bb1ac&ip=$ip&format=json";

                $d = file_get_contents($url);
                $data = json_decode($d , true);

/*
{
"statusCode" : "OK",
"statusMessage" : "",
"ipAddress" : "74.125.45.100",
"countryCode" : "US",
"countryName" : "UNITED STATES",
"regionName" : "CALIFORNIA",
"cityName" : "MOUNTAIN VIEW",
"zipCode" : "94043",
"latitude" : "37.3956",
"longitude" : "-122.076",
"timeZone" : "-08:00"
}
*/

$info =  array();

if(strlen($data['countryCode']))
{
    $info = array(
        'ip' => $data['ipAddress'] ,
        'country_code' => $data['countryCode'] ,
        'country_name' => $data['countryName'] ,
        'region_name' => $data['regionName'] ,
        'city' => $data['cityName'] ,
        'zip_code' => $data['zipCode'] ,
        'latitude' => $data['latitude'] ,
        'longitude' => $data['longitude'] ,
        'time_zone' => $data['timeZone'] ,
    );
}


?>
<?php if (isset($info['city']) && $info['city'] != '-'): ?>
    <a id="locate-my-city" href="https://www.google.co.in/maps/@18.9923328,72.822784" data-city="<?php echo $info['city']; ?>" target="_blank"> <span>locate my city <div class="scircular-target"></div></span></a>
<?php else: ?>
    <a id="locate-my-city" href="https://www.google.co.in/maps/@18.9923328,72.822784" data-city="all" target="_blank"> <span>locate my city <div class="scircular-target"></div></span></a>
<?php endif ?>


<div class="dropdown">
    <form id="vint">
        <select id="locate-city" name="locate">
            <option value="">All Cities</option>
            <option value="bengaluru">Bengaluru</option>
            <option value="chennai">Chennai</option>
            
            <option value="gurgaon">Gurgaon</option>
            <option value="kolkata">Kolkata</option>
            <option value="mumbai">Mumbai</option>
            <option value="newdelhi">New Delhi</option>
            <option value="noida">Noida</option>                        
            <option value="pune">Pune</option>
        </select>
    </form>
</div>
</div>

<div class="hr-line"></div>
<ul id="locate">

    <li class="newdelhi wow fadeInUp">
        <div class="item">
            <h5>Ambience Mall</h5>
            <span class="location"><img src="images/svg/location.svg">Vasant Kunj, New Delhi</span>
            <span class="location"><img src="images/svg/phone.svg"> <a href="tel: 011-40870055" > 011-40870055</a></span>
            <span class="nxt"><a class="tabbtn next-step" target="_blank" href="https://www.google.co.in/maps/place/Ambience+Mall+Vasant+Kunj/@28.5412132,77.1527009,17z/data=!3m1!4b1!4m5!3m4!1s0x390d1dbecef239fd:0xae39b6e3adbb268b!8m2!3d28.5412132!4d77.1548896"><span>GET DIRECTIONS <div class="sright-arrow-black arrow-c"></div></span></a></span>
        </div>
    </li>
    <li class="newdelhi wow fadeInUp">
        <div class="item">
            <h5>Pacific Mall</h5>
            <span class="location"><img src="images/svg/location.svg"> <span> Najafgarh Road, New Delhi</span> </span>
            <span class="location"><img src="images/svg/phone.svg"> <a href="tel:8046524224" > 011-40195742</a></span>
            <span class="nxt"><a class="tabbtn next-step" target="_blank" href="https://www.google.co.in/maps/place/Pacific+Mall/@28.637067,76.6416139,9.43z/data=!4m8!1m2!2m1!1sPacific+Mall!3m4!1s0x390d0363c4edf747:0x240c75aa3d09de7a!8m2!3d28.6425226!4d77.1062816"><span>GET DIRECTIONS <div class="sright-arrow-black arrow-c"></div></span></a></span>
        </div>
    </li>

    <li class="newdelhi wow fadeInUp">
        <div class="item">
            <h5>Select CITYWALK</h5>
            <span class="location"><img src="images/svg/location.svg"> Saket, New Delhi</span>
            <span class="location"><img src="images/svg/phone.svg"> <a href="tel:011-45503094" > 011-45503094</a> </span>
            <span class="nxt"><a class="tabbtn next-step" target="_blank" href="https://www.google.co.in/maps/place/Select+CITYWALK/@28.5284957,77.2169163,17z/data=!3m2!4b1!5s0x390ce18b1f46ef05:0xd23b80b99ddb1f67!4m5!3m4!1s0x390ce18ac60e4a61:0x81c366c7998e72a3!8m2!3d28.528491!4d77.219105"><span>GET DIRECTIONS <div class="sright-arrow-black arrow-c"></div></span></a></span>
        </div>
    </li>

    <li class="noida wow fadeInUp">
        <div class="item">
            <h5>DLF Mall of India</h5>
            <span class="location"><img src="images/svg/location.svg"> Noida</span>
            <span class="location"><img src="images/svg/phone.svg"> <a href="tel:0120-6714632" > 0120-6714632</a></span>
            <span class="nxt"><a class="tabbtn next-step" target="_blank" href="https://www.google.co.in/maps/place/DLF+Mall+of+India/@28.567546,77.3190643,17z/data=!3m1!4b1!4m5!3m4!1s0x390ce44f4d1b2a95:0x82fbbe415b8fedbe!8m2!3d28.5675413!4d77.321253"><span>GET DIRECTIONS <div class="sright-arrow-black arrow-c"></div></span></a></span>
        </div>
    </li>


    <li class="gurgaon wow fadeInUp">
        <div class="item">
            <h5>Ambience Mall </h5>
            <span class="location"><img src="images/svg/location.svg"> Gurgaon, Haryana</span>
            <span class="location"><img src="images/svg/phone.svg"> <a href="tel:0124-4665657" > 0124-4665657</a> </span>
            <span class="nxt"><a class="tabbtn next-step" target="_blank" href="https://www.google.co.in/maps/place/Ambience+Mall,+Gurugram/@28.5053725,77.0942228,17z/data=!3m1!4b1!4m5!3m4!1s0x390d1938456789c7:0x45a757aa37e73026!8m2!3d28.5053725!4d77.0964115"><span>GET DIRECTIONS <div class="sright-arrow-black arrow-c"></div></span></a></span>
        </div>
    </li>

    <li class="kolkata wow fadeInUp">
        <div class="item">
            <h5>Quest Mall</h5>
            <span class="location"><img src="images/svg/location.svg"> Ballygunge, Kolkata</span>
            <span class="location"><img src="images/svg/phone.svg"> <a href="tel:033-40054462" > 033-40054462</a> </span>
            <span class="nxt"><a class="tabbtn next-step" target="_blank" href="https://www.google.co.in/maps/place/Quest+Mall/@22.5390707,88.3634817,17z/data=!3m1!4b1!4m5!3m4!1s0x3a0276dfad4bf901:0xfbbe370e3a5a4dc8!8m2!3d22.5390658!4d88.3656704"><span>GET DIRECTIONS  <div class="sright-arrow-black arrow-c"></div></span></a></span>
        </div>
    </li>

    <li class="kolkata wow fadeInUp">
        <div class="item">
            <h5>South City Mall</h5>
            <span class="location"><img src="images/svg/location.svg"> <span class="inner-text">Prince Anwar Shah Road, Kolkata</span> </span>
            <span class="location"><img src="images/svg/phone.svg"> <a href="tel:033-40054462" > Coming Soon</a> </span>
            <span class="nxt"><a class="tabbtn next-step" target="_blank" href="https://www.google.co.in/maps/place/South+City+Mall,+375,+Prince+Anwar+Shah+Rd,+South+City+Complex,+Jadavpur,+Kolkata,+West+Bengal+700068/@22.5013698,88.3596175,17z/data=!3m1!4b1!4m5!3m4!1s0x3a0270d86189449f:0x484186387d357892!8m2!3d22.5015119!4d88.361723"><span>GET DIRECTIONS  <div class="sright-arrow-black arrow-c"></div></span></a></span>
        </div>
    </li>


    <li class="mumbai wow fadeInUp">
        <div class="item">
            <h5>Palladium</h5>
            <span class="location"><img src="images/svg/location.svg"> Lower Parel, Mumbai</span>
            <span class="location"><img src="images/svg/phone.svg"><a href="tel: 022-43470436" > 022-43470436</a></span>
            <span class="nxt"><a class="tabbtn next-step" target="_blank" href="https://www.google.co.in/maps/place/Palladium/@18.9954623,72.8229992,17z/data=!3m1!5s0x3be7ce8c7ac285bf:0xbbf7fd84645983e5!4m8!1m2!2m1!1sPALLADIUM!3m4!1s0x3be7ce8c79e8b109:0xa69a7761c3b1cd20!8m2!3d18.9942389!4d72.824126"><span>GET DIRECTIONS <div class="sright-arrow-black arrow-c"></div></span></a></span>
        </div>
    </li>



    <li class="pune wow fadeInUp">
        <div class="item">
            <h5>The Pavillion</h5>
            <span class="location"><img src="images/svg/location.svg"> S B Road, Pune</span>
            <span class="location"><img src="images/svg/phone.svg"> <a href="tel:020-66421091" > 020-66421091</a> </span>
            <span class="nxt"><a class="tabbtn next-step" target="_blank" href="https://www.google.co.in/maps/place/The+Pavillion/@18.5397277,73.6663939,10.64z/data=!4m8!1m2!2m1!1sTHE+PAVILLION!3m4!1s0x3bc2bf70ab733f0d:0x1bcd0c30e19208a3!8m2!3d18.5339882!4d73.8299274"><span>GET DIRECTIONS <div class="sright-arrow-black arrow-c"></div></span></a></span>
        </div>
    </li>




    <li class="bengaluru wow fadeInUp">
        <div class="item">
            <h5>VR Bengaluru</h5>
            <span class="location"><img src="images/svg/location.svg"> <span class="inner-text">Whitefield Main Road, Bengaluru</span>  </span>
            <span class="location"><img src="images/svg/phone.svg"> <a href="tel:8046524224" > 8046524224</a></span>
            <span class="nxt"><a class="tabbtn next-step" target="_blank" href="https://www.google.co.in/maps/place/VR+Bengaluru/@12.9963906,77.6931204,17z/data=!3m1!4b1!4m5!3m4!1s0x3bae110b40895db5:0x1de11baa6dc281a7!8m2!3d12.9963854!4d77.6953091"><span>GET DIRECTIONS <div class="sright-arrow-black arrow-c"></div></span></a></span>
        </div>
    </li>
    <li class="chennai wow fadeInUp">
        <div class="item">
            <h5>Palladium</h5>
            <span class="location"><img src="images/svg/location.svg"> <span class="inner-text">Velachery Road, Chennai</span>  </span>
            <span class="location"><img src="images/svg/phone.svg"> <a href="tel:8046524224" > Coming Soon</a></span>
            <span class="nxt"><a class="tabbtn next-step" target="_blank" href="https://www.google.co.in/maps/place/Palladium/@12.992963,80.2156569,17z/data=!3m1!4b1!4m5!3m4!1s0x3a526763a15512c9:0x6b3b09779cb3a2a2!8m2!3d12.9929578!4d80.2178456"><span>GET DIRECTIONS <div class="sright-arrow-black arrow-c"></div></span></a></span>
        </div>
    </li>









</ul>


</div>
</div>
<img class="shoes-ladies" src="images/shoes-ladies.png">
<img class="shoes-man" src="images/shoes-man.png">
</div>