
<div class="hr-line"></div>
<footer>
	<div >
		<div class="inst-title">
			follow us
		</div>    
	</div>    
	<div class="social wow fadeInUp">

		<ul class="social-icon">
			<li class="facebook-f">
				<a target="_blank" href=" https://www.facebook.com/DuneLondonIndia/?hc_ref=ARRWM79exUZnME_nudxTcqe939zYNaGQcOQN7P1eT5FTYhhL0hqrhBMDIOWRA0xi0CU&fref=nf">
					<p>
						<i class="fa fa-facebook"></i>
					</p>
					<span>facebook</span>
				</a>
			</li>
			<li class="instagram-f">
				<a target="_blank" href="https://www.instagram.com/dune_london_india/">
					<p>
						<i class="fa fa-instagram"></i>
					</p>
					<span>instagram</span>
				</a>
			</li>
		</ul>

	</div>
	<div class="footer">
		Copyright &copy; 2018 Reliance Brands Ltd. All rights reserved <span>|</span><br> Crafted by  <a href="https://www.togglehead.in/" target="_blank">Togglehead</a> 
	</div>
</footer>


<a href="#top2" class="back-to-top"><!-- <i class="fa fa-arrow-up"></i> -->
	
	<div  class="top-button-white stop-button-white"></div>
	<div  class="top-arrow stop-arrow"></div></a>




	<!--build:js js/main.min.js -->
	<script src="js/lib/jquery-3.2.1.min.js"></script>
	<script src="js/lib/slick.min.js"></script>
	<script src="js/lib/jquery-ui.min.js"></script>
	<script src="js/lib/modernizr-3.5.0.min.js"></script>
	<script src="js/lib/jquery.validate.min.js"></script>
	<script src="js/lib/wow.min.js"></script>
	<script src="js/main.js"></script>


	<!-- endbuild -->
	

</body>
</html>