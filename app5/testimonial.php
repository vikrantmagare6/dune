
<div class="testimonial">

  <div class="inner parallax">

    <div class="test-title">
      <div class="inst-title">
        Customers speak
      </div>    
    </div>    

    <div class="testimobial-slide">
      <div class="item">
        <div class="outer">
          <div class="inner">
            <div class="text">
              <p><span  class="left-qote sleft-qote"></span> <span class="sright-qote right-qote"></span>
               I have been using Dune London’s products for a year and a half now &amp; the brand has gone on to dominate 80% of my wardrobe as well as my Husband’s. The products are amazing!

             </p>
             <span>Monika Lunia </span>
           </div>
         </div>
       </div>
     </div>
     <div class="item">
      <div class="outer">
        <div class="inner">
          <div class="text">
            <p><span  class="left-qote sleft-qote"></span> <span class="sright-qote right-qote"></span>
              I found the perfect handbag along with a matching pair of shoes that go with every outfit in my wardrobe!
            </p>
            <span>Rhea Manik</span>
          </div>
        </div>
      </div>
    </div>
    <div class="item">
      <div class="outer">
        <div class="inner">
          <div class="text">
            <p><span  class="left-qote sleft-qote"></span> <span class="sright-qote right-qote"></span>
              Being a workaholic, I need shoes that I can wear for long hours without compromising on style; Dune London’s Ballerinas solve this problem for me.
            </p>
            <span>Shivani Behl</span>
          </div>
        </div>
      </div>
    </div>
    <div class="item">
      <div class="outer">
        <div class="inner">
          <div class="text">
            <p><span  class="left-qote sleft-qote"></span> <span class="sright-qote right-qote"></span>
              Dune’s shoes are extremely comfortable &amp; they fit beautifully even though I have wide feet!
            </p>
            <span>Kritika Sharma </span>
          </div>
        </div>
      </div>
    </div>
    
  </div>
</div>
</div>