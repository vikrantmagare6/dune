<!-- Hidden video div -->
<div style="display:none;" id="video1">
  <video class="lg-video-object lg-html5 video-js vjs-default-skin" controls preload="none">
    <source src="http://redarchitects.in/reD-intro.mov" type="video/mp4">
     Your browser does not support HTML5 video.
   </video>
 </div>

 <div id="about-us" class="about-us calcscrolldiv">
  <img class="bag-ladies" src="images/bag.png">
  <div class="inner-div">
    <div class="about-left-sec  ">
      <span class="processdiv_i inview">
        <h3>about us</h3>
        <div class="l-1"><i></i></div>

        <div class="l-3"><i></i></div>    
        <div class="l-4"><i></i></div>
      </span>

      <p>
        With an exceptional array of styles, the Dune brand is sophisticated and stylish, aspirational and unique and there really is something for everyone and for every occasion. From killer heels to chic everyday ballerina flats, statement boots and the perfect court shoe that will take you from day 
        to night in effortless style, Dune provides the perfect finishing touch 
        to any outfit.
      </p>	
    </div>
    <div class="video">
      <video id="ytvideo1" controls width="100%">
       <source src="http://redarchitects.in/reD-intro.mov" type="video/mp4">
       </video>
       <div class="video-poster">
         <div class="outer-valign">
           <div class="inner-valign">
            <div class="text-center" id="video-gallery">
              <div class="video-button" data-sub-html="AV Video" data-html="#video1" >
               <div class="triangle"><span></span></div>
               <div class="video-button-hover"></div>
             </div>
           </div>
         </div>
       </div>
     </div>
   </div>
 </div>
</div>