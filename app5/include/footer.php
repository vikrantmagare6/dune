</body>    

<!--build:js js/main.min.js -->
<script src="js/lib/jquery-3.2.1.min.js"></script>
<script src="js/lib/slick.min.js"></script>
<script src="js/lib/bootstrap.min.js"></script>
<script src="js/lib/jquery-ui.min.js"></script>
<script src="js/lib/modernizr-3.5.0.min.js"></script>
<script src="js/lib/jquery.validate.min.js"></script>
<script src="js/lib/validation.js"></script>
<script src="js/main.js"></script>
<script src="js/lib/lightgallery.min.js"></script>
<script src="js/lib/lg-video.min.js"></script>
<!-- endbuild -->

<link href="http://vjs.zencdn.net/4.12/video-js.css" rel="stylesheet">
<script src="http://vjs.zencdn.net/4.12/video.js"></script>
</body>

</html>