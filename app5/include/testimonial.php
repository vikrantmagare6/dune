
<div class="testimonial">
    <div class="inner parallax">
        <div class="testimobial-slide">
            <div class="item">
                <div class="outer">
                    <div class="inner">
                        <div class="text">
                            <p><img class="left-qote" src="/images/left-qote.png"><img class="right-qote" src="/images/right-qote.png">
                             From day to night, Dune has the perfect mix of heels, from cool classics to glam party shoes. There's always something for every occasion. 

                         </p>
                         <span>Jodie Nellist, Marie Claire</span>
                     </div>
                 </div>
             </div>
         </div>
         <div class="item">
            <div class="outer">
                <div class="inner">
                    <div class="text">
                        <p><img class="left-qote" src="/images/left-qote.png"><img class="right-qote" src="/images/right-qote.png">
                            From day to night, Dune has the perfect mix of heels, from cool classics to glam party shoes. There's always something for every occasion. 
                        </p>
                        <span>Jodie Nellist, Marie Claire</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="outer">
                <div class="inner">
                    <div class="text">
                        <p><img class="left-qote" src="/images/left-qote.png"><img class="right-qote" src="/images/right-qote.png">
                            From day to night, Dune has the perfect mix of heels, from cool classics to glam party shoes. There's always something for every occasion. 
                        </p>
                        <span>Jodie Nellist, Marie Claire</span>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</div>
</div>