<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title></title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="manifest" href="site.webmanifest">
  <link rel="apple-touch-icon" href="icon.png">
  <link href="https://fonts.googleapis.com/css?family=Raleway:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
  <!-- Place favicon.ico in the root directory -->
  <title></title>
  <link rel="stylesheet" href="http://sachinchoolur.github.io/lightGallery/lightgallery/css/lightgallery.css">
  <link rel="stylesheet" href="http://vjs.zencdn.net/4.12/video-js.css">
  <link rel='stylesheet' id='font-awesome-css'  href='css/font-awesome.min.css' type='text/css' media='all' />


  <!--build:css css/styles.min.css-->
  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/jquery-ui.min.css">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/slick.css">
  <link rel="stylesheet" href="css/slick-theme.css">
  <link rel="stylesheet" href="css/animate.css">
  <link rel="stylesheet" href="css/style.css">
  <!--endbuild-->

</head>
<body>
  <header>
   <div id="top" class="header">
    <a class="about-us-link" href="#about-us">about us</a>
    <a class="logo"><img src="/images/logo-white.png"></a>
    <a class="locate-us-link" href="#locate-us">locate us</a>
  </div>
</header>