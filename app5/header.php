<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Dune London</title>
  <meta name="description" content="">
  


  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <link rel="manifest" href="site.webmanifest">
  <link rel="apple-touch-icon" href="icon.png">
  <link href="https://fonts.googleapis.com/css?family=Raleway:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
  <!-- Place favicon.ico in the root directory -->
  <title>Dune London</title>


  <link rel="apple-touch-icon" href="/apple-touch-icon.png" />
  <link rel="canonical" href="https://www.dunelondon.com/" />
  <link rel="shortcut icon" href="https://www.dunelondon.com/favicon.ico" /> 



  
  
  

  
  <!--build:css css/styles.min.css-->
  <link rel='stylesheet' id='font-awesome-css'  href='css/font-awesome.min.css' type='text/css' media='all' />
  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/jquery-ui.min.css">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/slick.css">
  <link rel="stylesheet" href="css/slick-theme.css">
  <link rel="stylesheet" href="css/animate.css">
  <link rel="stylesheet" href="css/style.css">
  <!--endbuild-->

</head>
<body>

 <!--  <a class="about-us-link on-hover-line" href="#about-us">
    <span>about us</span>
    <div class="l-1"><i></i></div>
    <div class="l-2"><i></i></div>
    <div class="l-3"><i></i></div>    
    <div class="l-4"><i></i></div>
  </a>

  <br>
  <br>
  <hr> -->
  
  <header>
    <div id="top2" ></div>
    <div id="top" class="header wow fadeInUp">
      <div class="inner-div">
        <a class="about-us-link on-hover-line" href="#about-us">
          <span>about us</span>
          <div class="l-1"><i></i></div>
          <div class="l-2"><i></i></div>
          <div class="l-3"><i></i></div>    
          <div class="l-4"><i></i></div>
        </a>
        <a class="logo" data-df="#top"><img src="images/logo-white1.png"></a>
        <a class="locate-us-link on-hover-line" href="#locate-us">
          <span>locate us</span>
          <div class="l-1"><i></i></div>
          <div class="l-2"><i></i></div>
          <div class="l-3"><i></i></div>    
          <div class="l-4"><i></i></div>
        </a>  
      </div>
      
    </div>
  </header>

