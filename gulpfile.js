var gulp = require('gulp');
var connect = require ('gulp-connect-php');
var sass = require('gulp-sass');
var browserSync = require('browser-sync');
var useref = require('gulp-useref');
var uglify = require('gulp-uglify');
var gulpIf = require('gulp-if');
var cssnano = require('gulp-cssnano');
var imagemin = require('gulp-imagemin');
const sourcemaps = require('gulp-sourcemaps');
const autoprefixer = require('gulp-autoprefixer');



var cache = require('gulp-cache');
var del = require('del');
var runSequence = require('run-sequence');

gulp.task ('connect-sync', function () {
 connect.server ({
   port : 8000 ,
   base : 'app' ,
   bin : 'C:/wamp64/bin/php/php5.6.25/php.exe' ,
   ini : 'C:/wamp64/bin/php/php5.6.25/php.ini'
 }, function () {
   browserSync ({
     proxy : 'http://localhost:8000',
     baseDir: 'app'
   });
 });
});

gulp.task('asf', () =>
  gulp.src('app/css/**/*.css')
  .pipe(sourcemaps.init())
  .pipe(autoprefixer())
  .pipe(concat('all.css'))
  .pipe(sourcemaps.write('.'))
  .pipe(gulp.dest('dist'))
  );

// Basic Gulp task syntax
gulp.task('hello', function() {
  console.log('Hello Zell!');
})

// Development Tasks 
// -----------------

// Start browserSync server
gulp.task('browserSync', function() {
  browserSync({
    server: {
      baseDir: 'app'
    }
  })
})

gulp.task('sass', function() {
  return gulp.src('app/scss/**/*.scss').pipe(sourcemaps.init())
  .pipe(autoprefixer())
 // Gets all files ending with .scss in app/scss and children dirs
    .pipe(sass().on('error', sass.logError)).pipe(sourcemaps.write('.')) // Passes it through a gulp-sass, log errors to console
    .pipe(gulp.dest('app/css')) // Outputs it in the css folder
    .pipe(browserSync.reload({ // Reloading with Browser Sync
      stream: true
    }));
  })

// Watchers
gulp.task('watch',  ['browserSync', 'sass'], function() {
  gulp.watch('app/scss/**/*.scss', ['sass']);
  gulp.watch('app/*.html', browserSync.reload);
  gulp.watch('app/js/**/*.js', browserSync.reload);
})

gulp.task('watchphp',  ['connect-sync', 'sass'], function() {
  gulp.watch('app/scss/**/*.scss', ['sass']);
  gulp.watch('app/*.html', browserSync.reload);
  gulp.watch('app/*.php', browserSync.reload);
  gulp.watch('app/js/**/*.js', browserSync.reload);
})

// Optimization Tasks 
// ------------------

// Optimizing CSS and JavaScript 
gulp.task('useref', function() {

  return gulp.src(['app/*.html','app/*.php','app/.htaccess'])
  .pipe(useref())
  .pipe(gulpIf('*.js', uglify()))
  .pipe(gulpIf('*.css', cssnano()))
  .pipe(gulp.dest('dist'));
});

// Optimizing Images 
gulp.task('images', function() {
  return gulp.src('app/images/**/*.+(png|jpg|jpeg|gif|svg)')
    // Caching images that ran through imagemin
    .pipe(cache(imagemin({
      interlaced: true,
    })))
    .pipe(gulp.dest('dist/images'))
  });

// Copying fonts 
gulp.task('fonts', function() {
  return gulp.src('app/fonts/**/*')
  .pipe(gulp.dest('dist/fonts'))
})
gulp.task('includea', function() {
  return gulp.src('app/include/**/*')
  .pipe(gulp.dest('dist/include'))
})


gulp.task('cache:clear', function (callback) {
  return cache.clearAll(callback)
})
// Cleaning 
gulp.task('clean', function() {
  return del.sync('dist').then(function(cb) {
    return cache.clearAll(cb);
  });
})

gulp.task('clean:dist', function() {
  return del.sync(['dist/**/*', '!dist/images', '!dist/images/**/*','!dist/include']);
});

gulp.task('clean:dist2', function() {
  return del.sync(['dist/**/*']);
});

// Build Sequences
// ---------------

gulp.task('default', function(callback) {
  runSequence(['sass', 'browserSync', 'watch'],
    callback
    )
})

gulp.task('build', function(callback) {
  runSequence(
    'clean:dist',
    'sass',
    ['useref', 'images', 'fonts','includea'],
    callback
    )
})

gulp.task('build2', function(callback) {
  runSequence(
    'clean:dist2',
    'sass',
    ['useref'],
    callback
    )
})
